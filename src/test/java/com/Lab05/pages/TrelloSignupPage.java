package com.Lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

/**
 * Brought to life by Adela on 5/19/2018.
 */
@DefaultUrl("https://trello.com/signup")
public class TrelloSignupPage extends PageObject{
    @FindBy(name="name")
    private WebElementFacade name;

    @FindBy(name="email")
    private WebElementFacade email;

    @FindBy(name="password")
    private WebElementFacade password;

    @FindBy(id="signup")
    private WebElementFacade signupButton;

    public void doRegister(String name, String email, String password) {
        this.name.type(name);
        this.email.type(email);
        this.password.type(password);
        this.signupButton.click();
    }
}
