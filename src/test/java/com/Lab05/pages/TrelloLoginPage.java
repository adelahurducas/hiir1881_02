package com.Lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

/**
 * Brought to life by Adela on 5/19/2018.
 */
@DefaultUrl("https://trello.com/login")
public class TrelloLoginPage  extends PageObject{
    @FindBy(name="user")
    private WebElementFacade username;

    @FindBy(name="password")
    private WebElementFacade password;

    @FindBy(name="use-password")
    private WebElementFacade normalLogin;

    @FindBy(id="login")
    private WebElementFacade loginButton;

    public void doLogin(String username, String password) {
        this.username.type(username);
        this.password.type(password);
        this.loginButton.click();
    }

}
