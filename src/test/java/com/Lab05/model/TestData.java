package com.Lab05.model;

/**
 * Brought to life by Adela on 5/21/2018.
 */
public class TestData {
    private String goodUserLogin;
    private String goodPassLogin;
    private String badUserLogin;
    private String badPassLogin;
    private String goodNameRegister;
    private String goodEmailRegister;
    private String goodPassRegister;

    public String getGoodUserLogin() {
        return goodUserLogin;
    }

    public TestData setGoodUserLogin(String goodUserLogin) {
        this.goodUserLogin = goodUserLogin;
        return this;
    }

    public String getGoodPassLogin() {
        return goodPassLogin;
    }

    public TestData setGoodPassLogin(String goodPassLogin) {
        this.goodPassLogin = goodPassLogin;
        return this;
    }

    public String getBadUserLogin() {
        return badUserLogin;
    }

    public TestData setBadUserLogin(String badUserLogin) {
        this.badUserLogin = badUserLogin;
        return this;
    }

    public String getBadPassLogin() {
        return badPassLogin;
    }

    public TestData setBadPassLogin(String badPassLogin) {
        this.badPassLogin = badPassLogin;
        return this;
    }

    public String getGoodNameRegister() {
        return goodNameRegister;
    }

    public TestData setGoodNameRegister(String goodNameRegister) {
        this.goodNameRegister = goodNameRegister;
        return this;
    }

    public String getGoodEmailRegister() {
        return goodEmailRegister;
    }

    public TestData setGoodEmailRegister(String goodEmailRegister) {
        this.goodEmailRegister = goodEmailRegister;
        return this;
    }

    public String getGoodPassRegister() {
        return goodPassRegister;
    }

    public TestData setGoodPassRegister(String goodPassRegister) {
        this.goodPassRegister = goodPassRegister;
        return this;
    }
}
