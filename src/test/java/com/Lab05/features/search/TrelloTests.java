package com.Lab05.features.search;

import com.Lab05.model.TestData;
import com.Lab05.steps.serenity.EndUserSteps;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.*;

/**
 * Brought to life by Adela on 5/19/2018.
 */
@RunWith(SerenityRunner.class)
public class TrelloTests {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;
    public TestData testData;

    @Before
    public void initData() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("items.json").getFile());

        testData = new ObjectMapper().readValue(file, TestData.class);
    }

    @Steps
    public EndUserSteps anna;

    @Test
    public void login_valid() {
        anna.loginTrelloWithCredentials(testData.getGoodUserLogin(), testData.getGoodPassLogin());
    }

    @Test
    public void login_nonvalid() {
        anna.loginTrelloWithCredentials(testData.getBadUserLogin(), testData.getBadPassLogin());
    }

    @Test
    public void register_valid() {
        anna.registerTrello(testData.getGoodNameRegister(),testData.getGoodEmailRegister(), testData.getGoodPassRegister());
    }

    @Test
    public void register_nonvalid() {
        anna.registerTrello(testData.getGoodNameRegister(),testData.getGoodEmailRegister(), testData.getGoodPassRegister());

    }
}
