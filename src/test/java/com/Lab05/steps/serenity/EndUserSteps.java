package com.Lab05.steps.serenity;

import com.Lab05.pages.DictionaryPage;
import com.Lab05.pages.TrelloLoginPage;
import com.Lab05.pages.TrelloSignupPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    TrelloLoginPage trelloLoginPage;

    TrelloSignupPage trelloSignupPage;

    @Step
    public void enters(String keyword) {
        dictionaryPage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        dictionaryPage.lookup_terms();
    }

    @Step
    public void should_see_definition(String definition) {
        assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition)));
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }

    @Step
    public void openPage() {
        trelloLoginPage.open();
    }

    @Step
    public void loginTrelloWithCredentials(String username, String password) {
        trelloLoginPage.open();
        trelloLoginPage.doLogin(username, password);
    }

    @Step
    public void registerTrello(String name, String email, String password){
        trelloSignupPage.open();
        trelloSignupPage.doRegister(name,email,password);
    }


}